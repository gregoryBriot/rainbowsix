<img src="images/imageAccueil.jpg" class="v-banner" style="width:100%;"></img>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-9 col-sm-12 col-lg-9">
            <div class="row">
                <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
                    <img src="images/3FactionsGauche.png"  class="img-responsive img-rounded PaddingImageIndex imagesFactions d-none d-md-block">
                </div>
                <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">
                    <p class="contenuIndex Center">Bienvenue sur le site de TapTap Heroes</p>
                </div>
                <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
                    <img src="images/3FactionsDroite.png"  class="img-responsive img-rounded PaddingImageIndex imagesFactions d-none d-md-block">
                </div>
                <div class="row paddingContenuIndex">
                    <div class="col-md-12">
                        <h2 class="Center textContenu backgroundContenuIndex borderContenu">Description de l'histoire de TapTapHeroes</h2>
                        <p class="Center textContenu backgroundTitre borderContenu">Tap Tap Heroes vous emmene sur le continent de Mystia où plusieurs héros se rassemblent.
                            Sur Mystia, une épée sacrée qui contient le pouvoir de la création a été découverte par Freya, la Reine de l’enfer.
                            En conséquence, Freya a l'intention d'exploiter la puissance de l'épée sacréeet de prendre le contrôle du monde.
                            Les Chevaliers de l'Alliance dans leurs châteaux, les Guerriers de la Horde dans leurs cavernes, les Mages Elfiques dans leurs forêts,
                            les Morts-vivants dans leurs tombes et les créatures saintes aux cieux, tous commencent leurs voyages pour trouver et acquérir le pouvoir de la création.
                            Avec plus de 200 héros à travers six factions différentes, vous aussi devez trouver l'épée sacrée et neutraliser les plans de Freya avant qu'il ne soit trop tard.</p>

                        <h2 class="Center textContenu backgroundContenuIndex borderContenu">Principe de ce jeu</h2>
                        <p class="Center textContenu backgroundTitre borderContenu">
                            TapTap Heroes est un jeu de rôle dans lequel on peut acquerir des personnages avec des compétences et des statistiques uniques. Ce jeu dispose d'un mode histoire,
                            d'un mode d'expédition et bien d'autres encore, ces modes sont là pour faire progressser nos héroes. Les héroes ont tous une faction et un type, il existe 6 factions
                            et 5 types. Le but du jeu est de devenir le meilleur de notre serveur attitré, plus on joue plus on progresse.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3" style="padding-top: 20%" >
            <div class="borderSidebar backgroundSideBar">
                <p class="text souligner color">A Propos:</p>
                <p class="text">
                    Ce site est en l'honneur de l'univers de TapTap Heroes
                </p>
                <p class="text paddingsidebar souligner color">Sites favoris:</p>
                <div class="Center">
                    <a href="https://www.facebook.com/WestbundGame/" class="Link" target="_blank"> Facebook</a>
                    <br>
                    <a href="https://twitter.com/taptapheroes?lang=fr" class="Link" target="_blank"> Twitter</a>
                    <br>
                    <a href="https://play.google.com/store/apps/details?id=com.westbund.heros.en&hl=fr&gl=US" class="Link" target="_blank"> Téléchargement pour Android</a>
                    <br>
                    <a href="https://apps.apple.com/fr/app/taptap-heroes-cage-du-vide/id1369678805" class="Link" target="_blank"> Téléchargement pour Iphone</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
