<section class="ftco-section">
		<div class="container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<nav class="navbar navbar-expand-lg  ftco-navbar-light" id="ftco-navbar">
		    <div class="container">
		    	<a class="navbar-brand" href="index.html">Rainbow Six: Siege</a>
		    	<div class="social-media order-lg-last">
		    		<p class="mb-0 d-flex">
		    			<a href="https://www.facebook.com/Rainbow6France/" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
		    			<a href="https://twitter.com/Rainbow6FR?s=20" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
		    			<a href="https://www.instagram.com/rainbowsix_fr/?hl=fr" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
		    		</p>
	        </div>
		      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
		        <span class="fa fa-bars"></span> Menu
		      </button>
		      <div class="collapse navbar-collapse" id="ftco-nav">
		        <ul class="navbar-nav ml-auto mr-md-3">
		        	<li class="nav-item active"><a href="#" class="nav-link">Accueil</a></li>
		        	<li class="nav-item"><a href="Apropos.php" class="nav-link">A propos</a></li>
		        	<li class="nav-item"><a href="Personnages.php" class="nav-link">Personnages</a></li>
		        	<li class="nav-item"><a href="Armes.php" class="nav-link">Armes</a></li>
                    <li class="nav-item"><a href="Cartes.php" class="nav-link">Cartes</a></li>
		          <li class="nav-item"><a href="Telecharger.php" class="nav-link">Télécharger</a></li>
		        </ul>
		      </div>
		    </div>
		  </nav>
    <!-- END nav -->
  </div>

	</section>

	<script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>